FROM alpine:3.16.0

ARG LOG_FILE=httpd.log
ENV LOG_FILE=${LOG_FILE}

ARG LOG_DB=httpd.db
ENV LOG_DB=${LOG_DB}

RUN apk add bash=5.1.16-r2
RUN apk add curl=7.83.1-r1
RUN apk add python3=3.10.4-r0
RUN apk add sqlite=3.38.5-r0
RUN apk add py3-pip=22.1.1-r0
RUN pip3 install apache_log_parser

ENTRYPOINT [ "/bin/bash" ]

WORKDIR /logs

COPY a*.sh ./
RUN chmod +x a*.sh && ./all.sh

COPY sql/ ./sql/

ARG LOG_SOURCE=https://pastebin.com/raw/gstGCJv4
ENV LOG_SOURCE=${LOG_SOURCE}

COPY fetch.sh .
RUN chmod +x fetch.sh && ./fetch.sh

COPY load.py .
RUN chmod +x load.py && ./load.py

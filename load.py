#!/usr/bin/env python3

# See: https://bhoey.com/blog/parsing-apache-logs-into-a-database/
# See: https://github.com/amandasaurus/apache-log-parser
# See: https://httpd.apache.org/docs/current/mod/mod_log_config.html

# Considering default: LogFormat "%h %l %u %t \"%r\" %>s %b "
# Likely we have referer and a user agent
# Also, what size to set is not known (%b %B %O etc ...). Anyway it does not important for answers.
# Sample: 10.101.3.205 - - [25/Nov/2013:16:51:18 +0000] "GET /dev/file_content/plugins/puppet/provider/rvm_alias/alias.rb HTTP/1.1" 200 751 "-" "-"
# Full log: https://pastebin.com/gstGCJv4
 
import os
import sqlite3
import apache_log_parser

LOG_DB = os.environ.get('LOG_DB','/tmp/httpd.db')
LOG_FILE=os.environ.get('LOG_FILE','/tmp/httpd.log')
LOG_FORMAT="%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\""

conn = sqlite3.connect(LOG_DB)
cur = conn.cursor()
cur.execute("""
  CREATE TABLE IF NOT EXISTS logs (
    date TEXT,
    remote_host TEXT,
    request_method TEXT,
    request_url TEXT,
    status INTEGER
  );
""")
cur.execute("""
  DELETE FROM logs;
""")

parser = apache_log_parser.make_parser(LOG_FORMAT)
with open(LOG_FILE) as f:
  for line in f:
    d = parser(line)
    cur.execute("""
      INSERT INTO logs ( date, remote_host, request_method,  request_url,  status)
      VALUES (:time_received_tz_isoformat, :remote_host, :request_method, :request_url, :status)
    """, d)

cur.close()
conn.commit()
conn.close()

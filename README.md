# btlabz-docker-httpd2sqlite-poc

## Overview

This is a POC for ad-hoc apache log analysis. So we leverage:

- apache log parser library for parsing logs
- sqlite to store and query log data
- python as a glue
- docker as solution packaging

So we import logs to some database alike storage to query with a structured query language. Just like this:

```sql
select
  count(*)
from
  logs
where
  request_url like "/some/path/prefix/%"
;
```

## Requirements

- docker

## Runbook

Use the solution like this to get all the results:

```bash
git clone https://gitlab.com/btlabz-other/btlabz-docker-httpd2sqlite-poc.git
cd btlabz-docker-httpd2sqlite-poc
docker build -t logs:latest .
docker run -ti logs:latest /logs/all.sh
```

It's possible to run container and use scripts inside:

- [/logs/fetch.sh](fetch.sh) - refresh logs from the internet
- [/logs/load.py](load.py) - reload database
- /logs/a00x.sh - individual metrics
- [/logs/all.sh](all.sh) - all the metrics

Environment arguments:

- LOG_SOURCE - raw log address

Use like this:

```bash
docker build -t logs:latest --build-arg LOG_SOURCE=https://pastebin.com/raw/RthJwvSU .
docker run -ti logs:latest /logs/all.sh

```

## Metrics

- [a001](sql/a005.sql) - How many times the URL "/production/file_metadata/modules/ssh/sshd_config" was fetched
- [a002](sql/a005.sql) - Of those requests, how many times the return code from Apache was not 200
- [a003](sql/a005.sql) - The total number of times Apache returned any code other than 200
- [a004](sql/a005.sql) - The total number of times that any IP address sent a PUT request to a path under "/dev/report/"
- [a005](sql/a005.sql) - A breakdown of how many times such requests were made by IP address

## TODOs

- Parametrize log format (it's hardcoded now)
- GITLab docker ci

## Notice

It's possible to analyse logs with basic tools like grep\awk, or leverage some scription language.
However, it's can easely become complicated and error prone. E.g.:

```bash
cat httpd.log | grep -iEc "\s+/production/file_metadata/modules/ssh/sshd_config\?{0,1}\S*\s+"
```
```bash
cat httpd.log | grep -iE "\s+/production/file_metadata/modules/ssh/sshd_config\?{0,1}\S*\s+\S+\s+" | grep -iEvc "HTTP/1.1\" 200"
```

It's also possible to leverage self-hosted like ELK, Loki, Graylog.

Or some managed service. E.g. AWS CloudWatch Logs, DataDog, Dynatrace or Splunk to perform ad-hoc analisys.

Bigdata tools can provide options for huge log data. E.g. s3+athena, cassandra, presto.

## References

- [T1: apache log analisys with python, bash, sqlite and docker](https://gitlab.com/btlabz-other/btlabz-docker-httpd2sqlite-poc)
- [T2: terraform node](https://gitlab.com/btlabz-umami/terraform-aws-btlabz-umami-dev-layer) and [ansible playbook](https://gitlab.com/btlabz-umami/btlabz-ansible-umami-playbook)
- [T3: Umami HA and resilience considerations](https://gitlab.com/btlabz-umami/btlabz-docs-umami-ha)
- [T4: Umami monitoring considerations](https://gitlab.com/btlabz-umami/btlabz-docs-umami-monitoring)

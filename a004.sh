#!/bin/bash

echo 'The total number of times that any IP address sent a PUT request to a path under "/dev/report/"'

cat ./sql/a004.sql | sqlite3 httpd.db

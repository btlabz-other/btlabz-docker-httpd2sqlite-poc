select
  count(*)
from
  logs
where
  request_method in ("PUT")
  and request_url like "/dev/report/%"
;

select
  count(*)
from
  logs
where
  request_url like "/production/file_metadata/modules/ssh/sshd_config%"
  and status not in (200)
;

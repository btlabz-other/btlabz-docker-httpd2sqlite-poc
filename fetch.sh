#!/bin/bash

curl --silent --output ${LOG_FILE} --fail --retry 5 --retry-delay 5  --retry-all-errors --show-error --location ${LOG_SOURCE}

ls -la ${LOG_FILE}

md5sum ${LOG_FILE}
